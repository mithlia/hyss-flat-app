## Installation

Installation from **git** repository :

* Get the latest version of **hyssFlatApp** source code from [BitBucket](https://bitbucket.org/mithlia/hyss-flat-app):

    ```
    $ git clone https://bitbucket.org/mithlia/hyss-flat-app.git
    ```

* Upload all contents of **hyss-flat-app** directory to your server or host. You can upload the files in the root directory, or in a subdirectory such as **/hyss-flat-app/**.
* If **hyssFlatApp** is installed in a subdirectory, the **.htaccess** file need to be modified. If **hyssFlatApp** for example is installed in the subdirectory **hyss-flat-app**, the **.htaccess** file has to be modified as follows:

    ```
    RewriteBase /hyss-flat-app/
    ```

* Got to the installer page. If you uploaded the files in the root directory just go to **http://example.com** or **http://localhost**. If you uploaded the files in a subdirectory just go to **http://example.com/hyss-flat-app/** or **http://localhost/hyss-flat-app/**.
* Then follow the installer to setup your CMS.
* After successful install, you can log in to the admin dashboard to manage your contents at **http://example.com/backend** or **http://localhost/backend**

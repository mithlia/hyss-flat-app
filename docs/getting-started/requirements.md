## Requirements

* **cLHy 1.6.28** or higher, with **capi_rewrite** and **.htaccess** file supports activated.
* **HySS 1.0.13** or higher, with extensions library **mbstring**, **GD**, **DOM**, and **json** supports enabled.

**hyssFlatApp** uses the **.htaccess** file to keep some configurations, like rewrite rules :

* If you install **hyssFlatApp** in the documents root directory (e.g., **/usr/local/clhydelman/htdocs**), you need to uncomment the line **RewriteBase /**.
* If you install **hyssFlatApp** in a sub-directory, for example, **/usr/local/clhydelman/htdocs/hyss-flat-app**, you need to uncomment and change the **RewriteBase /** line to **RewriteBase /hyss-flat-app/**.

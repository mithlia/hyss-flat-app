## Introduction to hyssFlatApp API

**hyssFlatApp** is designed with API (Application Programming Interface) to provide easy way to integrate itself with other systems. With this API, you can retrieve or update data from the database with a simple HTTP request.

### Installation

**hyssFlatApp** comes with the API plugin pre-installed, you only need to activate it from plugins section.

	Admin panel > Plugins > API > Activate

### URL

The URL of the API is:

	{protocol}://{domain}/api/{endpoint}

Example:

	https://www.example.com/api/pages

### Endpoints and Methods
	endpoint		method		description
	/pages			GET		Returns a list of pages
	/pages/{page key}	GET		Returns a page by the page key
	/pages			POST		Create a new page
	/pages/{page key} 	PUT		Edit a page
	/pages/{page key}	DELETE		Delete a page
	/settings		GET		Returns the Bludit settings
	/settings		PUT		Edit Bludit settings
	/images			POST		Upload an image and generate the thumbnail for a page
	/tags			GET		Returns a list of tags and pages keys related to the tag
	/tags/{tag key}		GET		Returns a tag by tag key
	/categories		GET		Returns a list of categories and pages keys related to the category
	/categories/{categ key}	GET		Returns a category by category key
	/users			GET		Returns the list of users in the system
	/users/{username}	GET		Returns the profile user
	/files/{page key}	GET		Returns the files related to a page

### HTTP Response

The response content format is **Content-Type: application/json**. Default values for the body.

	key		type		description
	status		string		Returns 0 on success.
	message		string		Returns a little message about the execution.
	data		array		The content of the response for the endpoint.
	
### HTTP status code
	HTTP Code	Description
	200		Response successfull.
	400		Bad request, missing inputs.
	401		The API token or authentication token are missing or are wrong.

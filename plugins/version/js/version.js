
function getLatestVersion() {

	console.log("[INFO] [PLUGIN VERSION] Getting list of versions of hyssFlatApp.");

	$.ajax({
		url: "https://bitbucket.org/mithlia/hyss-flat-app/src/master/plugins/version/",
		method: "GET",
		dataType: 'json',
		success: function(json) {
			// Constant HYSSFLAT_BUILD is defined on variables.js
			if (json.stable.build > HYSSFLAT_BUILD) {
				$("#current-version").hide();
				$("#new-version").show();
			}
		},
		error: function(json) {
			console.log("[WARN] [PLUGIN VERSION] There is some issue to get the version status.");
		}
	});
}

getLatestVersion();

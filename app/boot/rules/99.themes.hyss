@!hyss defined('HYSSFLAT') or die('HySS Flat App.');

// ============================================================================
// Variables
// ============================================================================

// ============================================================================
// Functions
// ============================================================================

function buildThemes()
{
	global $site;

	$themes = array();
	$themesPaths = Filesystem::listDirectories(PATH_THEMES);

	foreach($themesPaths as $themePath)
	{
		// Check if the theme is translated.
		$languageFilename = $themePath.Slash.'languages'.Slash.$site->language().'.json';
		if( !Sanitize::pathFile($languageFilename) ) {
			$languageFilename = $themePath.Slash.'languages'.Slash.DEFAULT_LANGUAGE_FILE;
		}

		if( Sanitize::pathFile($languageFilename) )
		{
			$database = file_get_contents($languageFilename);
			$database = json_decode($database, true);
			if(empty($database)) {
				Log::set('99.themes.hyss'.LOG_SEP.'Language file error on theme '.$themePath);
				break;
			}

			$database = $database['theme-data'];

			$database['dirname'] = basename($themePath);

			// --- Metadata ---
			$filenameMetadata = $themePath.Slash.'metadata.json';

			if( Sanitize::pathFile($filenameMetadata) )
			{
				$metadataString = file_get_contents($filenameMetadata);
				$metadata = json_decode($metadataString, true);

				$database['compatible'] = false;
				if( !empty($metadata['compatible']) ) {
					$hyssflatappRoot = explode('.', HYSSFLAT_VERSION);
					$compatible = explode(',', $metadata['compatible']);
					foreach( $compatible as $version ) {
						$root = explode('.', $version);
						if( $root[0]==$hyssflatappRoot[0] && $root[1]==$hyssflatappRoot[1] ) {
							$database['compatible'] = true;
						}
					}
				}

				$database = $database + $metadata;
				array_push($themes, $database);
			}
		}
	}

	return $themes;
}

// ============================================================================
// Main
// ============================================================================

// Load the language file
$languageFilename = THEME_DIR.'languages'.Slash.$site->language().'.json';
if( !Sanitize::pathFile($languageFilename) ) {
	$languageFilename = THEME_DIR.'languages'.Slash.DEFAULT_LANGUAGE_FILE;
}

if( Sanitize::pathFile($languageFilename) )
{
	$database = file_get_contents($languageFilename);
	$database = json_decode($database, true);

	// Remote the name and description.
	unset($database['theme-data']);

	// Load words from the theme language
	if(!empty($database)) {
		$L->add($database);
	}
}

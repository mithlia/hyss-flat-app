## README

**hyssFlatApp** is a HySS application that uses flat files in JSON format to store and manage contents. It can be used to build your own CMS (websites, blogs, photo gallery, etc) in seconds.

### How to use?

You just need **cLHy** and **HySS** to run **hyssFlatApp** on your server.

* [Requirements](docs/getting-started/requirements.md)
* [Installation guide](docs/getting-started/installation.md)
* [API guide](docs/api/introduction.md)
* [Custom field contents](docs/content/custom-fields.md)
* [Screenshots](docs/screenshots)
* [LICENSE](LICENSE)
